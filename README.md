
```
#!php

include '/logger/log.class.php';
define('ROOT',dirname(__FILE__));

// -> ROOT.'/logs/events_mY.log'
Logger::name('events');

Logger::write('Event 1');
Logger::write('Event 2');

$arr = array( 'test' => true );
Logger::write( $arr,'json' );

```