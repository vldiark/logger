<?php
/**
 * Logger
 *
 * @name Logger
 * @author Vlad Ionov <vlad@f5.com.ru>
 * @version 1.03
 */
class Logger {
    /**
	 * log filename
     */
	private static $_name = 'events';
    /**
	 * log filename suffix
     */
	private static $_suff = '_mY';
    /**
	 * log path
     */
	private static $_path = 'logs';
    
	//=============================================
	
	/**
	 * Set log path
	 */
	public static function path( $dir ){

		if(!empty( $dir )) self::$_path = $dir;
	}
	
    /**
	 * Write to log
     */
	public static function write( $data, $type = 'text' ){
		
		if( $type == 'json' ){
			$data = json_encode( $data,JSON_UNESCAPED_UNICODE );
		}
		$data = date( 'd M Y H:i:s', time()).' - '.$data."\r\n";
		file_put_contents( ROOT.'/'.self::$_path.'/'.self::$_name.date( self::$_suff ).'.log', $data, FILE_APPEND | LOCK_EX );
	}
	
    /**
	 * Set log filename
     */
	public static function name( $name ){
		
		if(!empty( $name )) self::$_name = $name;
	}
}
